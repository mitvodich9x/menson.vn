﻿function StartCountDown(endDate, $timerCountDown) {
    /// <summary>Đếm ngược</summary>
    /// <param name="endDate" type="Date">Ngày kết thúc</param>
    /// <param name="$timerCountDown" type="Object">Đối tượng chứa khối đếm ngược được tìm qua jQuery</param>

    //Tính số giây còn lại
    var dNow = new Date();
    var totalSeconds = Math.floor((endDate - dNow) / 1000);

    //Gọi hàm đếm ngược theo số giây đã tính ra
    CountBack(totalSeconds, $timerCountDown);
}

function CountBack(totalSeconds, $timerCountDown) {
    if (totalSeconds > 0) {
        //Tính số ngày, giờ, phút, giây
        var days = Calcage(totalSeconds, 86400, 100000);
        var hours = Calcage(totalSeconds, 3600, 24);
        var minutes = Calcage(totalSeconds, 60, 60);
        var seconds = Calcage(totalSeconds, 1, 60);

        //Hiển thị con số ra html
        $("[data-name='days']", $timerCountDown).text(days);
        $("[data-name='hours']", $timerCountDown).text(hours);
        $("[data-name='minutes']", $timerCountDown).text(minutes);
        $("[data-name='seconds']", $timerCountDown).text(seconds);

        //Gọi lại hàm sau 1 giây
        setTimeout(function() {
                CountBack(totalSeconds - 1, $timerCountDown);
            },
            1000);
    } else {
        $("[data-name='days']", $timerCountDown).text(0);
        $("[data-name='hours']", $timerCountDown).text(0);
        $("[data-name='minutes']", $timerCountDown).text(0);
        $("[data-name='seconds']", $timerCountDown).text(0);
    }
}

function Calcage(secs, num1, num2) {
    var s = ((Math.floor(secs / num1)) % num2).toString();
    if (s.length < 2) {
        s = "0" + s;
    }
    return (s);
}



/***** Gọi đếm ngược cho các khối có class là timer-count-down và có thuộc tính data-end-date *****/
$(document)
    .ready(function() {
        $(".timer-count-down[data-end-date]")
            .each(function() {
                var endDate = new Date($(this).attr("data-end-date"));

                StartCountDown(endDate, $(this));
            });
    });
