﻿var clipboard = new ClipboardJS('.clipboard');

clipboard.on('success', function (e) {
    e.clearSelection();
    showTooltip(e.trigger, 'Copied!');
});

clipboard.on('error', function (e) {
    showTooltip(e.trigger, fallbackMessage(e.action));
});


//Phục vụ đánh dấu khi copied
var clipboards = document.querySelectorAll('.clipboard');

for (var i = 0; i < clipboards.length; i++) {
    clipboards[i].addEventListener('mouseleave', function (e) {
        e.currentTarget.setAttribute('class', 'clipboard notCopied');
    });
}

function showTooltip(elem, msg) {
    elem.setAttribute('class', 'clipboard');
}

// Simplistic detection, do not use it in production
function fallbackMessage(action) {
    var actionMsg = '';
    var actionKey = (action === 'cut' ? 'X' : 'C');

    if (/iPhone|iPad/i.test(navigator.userAgent)) {
        actionMsg = 'No support :(';
    }
    else if (/Mac/i.test(navigator.userAgent)) {
        actionMsg = 'Press ⌘-' + actionKey + ' to ' + action;
    }
    else {
        actionMsg = 'Press Ctrl-' + actionKey + ' to ' + action;
    }

    return actionMsg;
}