﻿// #region Đổi kiểu danh sách sản phẩm, đổi kiểu sắp xếp
function ChangeProductCateView(view) {
    AjaxLoading(true);
    //Gửi dữ liệu lên server
    $.ajax({
        url: WebUrl + "/Themes/Themes01/Ajax/Product.aspx",
        type: "POST",
        dataType: "json",
        data: {
            "action": "ChangeProductCateView",
            "view": view
        },
        success: function (res) {
            AjaxLoading(false);
            location.reload();
        },
        error: function (error) {
            AjaxLoading(false);
            console.log("Có lỗi xảy ra. ChangeProductCateView");
        }
    });
}

function ChangeProductCateOrder() {
    AjaxLoading(true);
    //Gửi dữ liệu lên server
    $.ajax({
        url: WebUrl + "/Themes/Themes01/Ajax/Product.aspx",
        type: "POST",
        dataType: "json",
        data: {
            "action": "ChangeProductCateOrder",
            "order": $("#slProductCateOrder").val()
        },
        success: function (res) {
            AjaxLoading(false);
            location.reload();
        },
        error: function (error) {
            AjaxLoading(false);
            console.log("Có lỗi xảy ra. ChangeProductCateOrder");
        }
    });
}
// #endregion

// #region Bỏ tích bộ lọc đã tìm ở trang kết quả tìm kiếm theo bộ lọc
function UnSelectFilter(filterId, value, isSearch, cateSeoUrl, searchKeyword) {
    var searchUrl = "";

    if (isSearch)
        searchUrl = WebUrl + cateSeoUrl + "/?keyword=";
    else
        searchUrl = WebUrl + cateSeoUrl + "/?keyword=" + searchKeyword;

    //Loại bỏ theo id
    $(".filters input[type='radio'][data-filterid='" + filterId + "'][value='" + value + "']").prop("checked", false);

    //Lặp tìm các thông số còn check để gán vào link tìm kiếm
    $(".filters input[type='radio']:checked")
        .each(function () {
            searchUrl += "&f-" + $(this).attr("data-filterid") + "=" + $(this).val();
        });

    window.location = searchUrl;
}
// #endregion

// #region Sự kiện khi thay đổi check ở bộ lọc tại trang danh sách sản phẩm
$(document)
    .ready(function () {
        $(".filters input[type='radio']")
            .change(function () {
                var searchUrl = WebUrl +
                    $("#hdCurrentCateSeoUrl").val() +
                    "/?keyword=" +
                    $("#hdCurrentSearchKeyword").val();

                $(".filters input[type='radio']:checked")
                    .each(function () {
                        searchUrl += "&f-" + $(this).attr("data-filterid") + "=" + $(this).val();
                    });

                window.location = searchUrl;
            });
    });
// #endregion

// #region Tăng giảm số lượng ở danh sách biến thể ở chi tiết sản phẩm
function QuantityUp(elm, number) {
    if (!number) number = 1;
    var $input = $(elm).parent().find("input");
    $input.autoNumeric("set",parseInt($input.autoNumeric("get")) + number).change();

    if (parseInt($input.autoNumeric("get")) < 0)
        $input.autoNumeric("set", 0).change();
}

function SumVariationMoney() {
    var total = 0;
    $(".table-variations tbody tr")
        .each(function() {
            var $tr = $(this);

            total += parseFloat($("[data-name='price']", $tr).attr("data-val")) *
                parseFloat($("input[data-name='quantity']", $tr).autoNumeric("get"));
        });

    $(".pd-total").autoNumeric("init", autoNumericSettings).autoNumeric("set", total);
}

$(document).ready(function() { SumVariationMoney() });
// #endregion

// #region Thêm vào danh sách yêu thích
function AddToWishlist(productId) {
    AjaxLoading(true);
    //Gửi dữ liệu lên server
    $.ajax({
        url: WebUrl + "/Themes/Themes01/Ajax/Product.aspx",
        type: "POST",
        dataType: "json",
        data: {
            "action": "AddToWishlist",
            "productId": productId
        },
        success: function (res) {
            AjaxLoading(false);
            alert(res[0]);
        },
        error: function (error) {
            AjaxLoading(false);
            console.log("Có lỗi xảy ra. AddToWishlist");
        }
    });
}
// #endregion

// #region Giỏ hàng

// #region Thêm nhiều sản phẩm vào giỏ hàng
function AddMultipleProductToShoppingCart(productId, goToShoppingCartPage) {
    /// <summary>Thêm sản phẩm vào giỏ hàng, các sản phẩm được thể hiện ở bảng với nhiều biến thể</summary>
    /// <param name="productId" type="Int">Id sản phẩm</param>
    /// <param name="goToShoppingCartPage" type="Boolean">True: thêm vào giỏ và tới trang giỏ hàng luôn.</param>

    var variations = [];

    $(".table-variations tbody tr")
        .each(function () {
            var $tr = $(this);

            if ($("input[data-name='quantity']", $tr).autoNumeric("get") > 0)
                variations.push({
                    "Id": $tr.attr("data-id"),
                    "Quantity": $("input[data-name='quantity']", $tr).autoNumeric("get")
                });
        });

    AjaxLoading(true);
    //Gửi dữ liệu lên server
    $.ajax({
        url: WebUrl + "/Themes/Themes01/Ajax/ProductCart.aspx",
        type: "POST",
        dataType: "json",
        data: {
            "action": "AddMultipleProductToShoppingCart",
            "productId": productId,
            "variations": JSON.stringify(variations)
        },
        success: function (res) {
            AjaxLoading(false);

            if (goToShoppingCartPage) {
                window.location = WebUrl + "san-pham/gio-hang";
            } else {
                alert(res[0]);

                // #region Lấy lại thông tin giỏ hàng
                GetShoppingCartList();
                // #endregion   
            }            
        },
        error: function (error) {
            AjaxLoading(false);
            console.log("Có lỗi xảy ra. AddMultipleProductToShoppingCart");
        }
    });
}
// #endregion

// #region Thay đổi số lượng sản phẩm trong giỏ hàng
function ChangeQuantityInShoppingCart(elm, productIdInShoppingCart) {
    /// <summary>Thay đổi số lượng sản phẩm trong giỏ hàng</summary>
    /// <param name="elm" type="Object">Truyền vào this</param>
    /// <param name="productIdInShoppingCart" type="Int">Id của sản phẩm, biến thể trong giỏ hàng</param>

    var quantity = $(elm).autoNumeric("get");
    
    if (quantity < 1) {
        if (!confirm("Bạn muốn xóa sản phẩm này khỏi giỏ hàng?"))
            quantity = 1;
    }

    AjaxLoading(true);
    //Gửi dữ liệu lên server
    $.ajax({
        url: WebUrl + "/Themes/Themes01/Ajax/ProductCart.aspx",
        type: "POST",
        dataType: "json",
        data: {
            "action": "ChangeQuantityInShoppingCart",
            "productId": productIdInShoppingCart,
            "quantity": quantity
        },
        success: function (res) {
            AjaxLoading(false);

            // #region Lấy lại thông tin giỏ hàng
            GetShoppingCartList();
            // #endregion
        },
        error: function (error) {
            AjaxLoading(false);
            console.log("Có lỗi xảy ra. ChangeQuantityInShoppingCart");
        }
    });
}

function RemoveProductInShoppingCart(productIdInShoppingCart) {
    /// <summary>Xóa sản phẩm trong giỏ hàng</summary>
    /// <param name="productIdInShoppingCart" type="Int">Id của sản phẩm, biến thể trong giỏ hàng</param>

    if (confirm("Bạn muốn xóa sản phẩm này khỏi giỏ hàng?")) {
        AjaxLoading(true);
        //Gửi dữ liệu lên server
        $.ajax({
            url: WebUrl + "/Themes/Themes01/Ajax/ProductCart.aspx",
            type: "POST",
            dataType: "json",
            data: {
                "action": "ChangeQuantityInShoppingCart",
                "productId": productIdInShoppingCart,
                "quantity": 0
            },
            success: function(res) {
                AjaxLoading(false);

                // #region Lấy lại thông tin giỏ hàng
                GetShoppingCartList();
                // #endregion
            },
            error: function(error) {
                AjaxLoading(false);
                console.log("Có lỗi xảy ra. ChangeQuantityInShoppingCart");
            }
        });
    }
}
// #endregion


// #region Gửi đơn hàng
function SendProductBooking(event) {
    if (event) event.preventDefault();

    //Khối chứa form
    var $container = $("#booking-form");

    //Validate dữ liệu
    if ($container.validate('check')) {
        AjaxLoading(true);

        //Khóa nút gửi để không bị gửi nhiều lần
        $("a[data-function='SendProductBooking']").css({ "pointer-events": "none" }).find(".btn-hong-lam-core").text("Đang gửi...");

        // #region Lấy địa chỉ nhận hàng, nếu chưa nhập quận huyện, tỉnh thành thì cộng thêm vào
        var addressReceiver = $("#tbDiaChi_bkf").val();
        var districtReceiverText = $("#slQuanHuyen option[value='" + $("#slQuanHuyen").val() + "']").text();
        var provinceReceiverText = $("#slTinhThanh option[value='" + $("#slTinhThanh").val() + "']").text();

        //if (addressReceiver.indexOf(districtReceiverText) < 0)
            addressReceiver += ", " + districtReceiverText;

        //if (addressReceiver.indexOf(provinceReceiverText) < 0)
            addressReceiver += ", " + provinceReceiverText;
        // #endregion

        // #region Lấy phương thức vận chuyển
        var paymentMethod = "";

        if ($(".pttt a[data-tab='ChuyenKhoan'].active").length > 0)
            paymentMethod = "Chuyển khoản";

        if ($(".pttt a[data-tab='COD'].active").length > 0)
            paymentMethod = "COD";

        if ($(".pttt a[data-tab='NhanTaiCuaHang'].active").length > 0) {
            paymentMethod = "Khách tới nhận tại của hàng: ";
            if ($(".tab-content[data-tab='NhanTaiCuaHang'] .agency-list .item.active").length > 0)
                paymentMethod += $(".tab-content[data-tab='NhanTaiCuaHang'] .agency-list .item.active")
                    .attr("data-value");
        }
        // #endregion

        //Gửi dữ liệu lên server
        $.ajax({
            url: "/themes/themes01/ajax/ProductCart.aspx",
            type: "POST",
            dataType: "json",
            data: {
                "action": "SendProductBooking",               
                "FullName": $("#tbHoTen_bkf").val(),
                "Email": $("#tbEmail_bkf").val(),
                "Mobile": $("#tbDienThoai_bkf").val(),
                "AddressReceiver": addressReceiver,
                "ProvinceReceiver": $("#slTinhThanh").val(),
                "DistrictReceiver": $("#slQuanHuyen").val(),
                "DescReceiver": $("#tbNoiDung_bkf").val(),
                "PaymentMethod": paymentMethod

            },
            success: function (res) {
                AjaxLoading(false);

                if (res[0] === "success") {
                    $(".form-alert", $container).CreateFormAlert(res[1], true, true);
                    $("a[data-function='SendProductBooking']").find(".btn-hong-lam-core").text("Đã gửi");
                } else {
                    $(".form-alert", $container).CreateFormAlert(res[1], false, true);
                    $("a[data-function='SendProductBooking']").css({ "pointer-events": "auto" }).find(".btn-hong-lam-core").text("Hoàn tất thanh toán");
                }
            },
            error: function (error) {
                AjaxLoading(false);
                console.log("Có lỗi xảy ra. SendProductBooking");
            }
        });
    }
}
// #endregion

// #endregion


// #region Quản lý đơn hàng của thành viên
// #region Lấy danh sách
function GetMemberProductOrder(itemPerPage, pageIndex) {
    //Khối chứa form
    var $container = $("#MemberProductOrderList");

    if ($container.length === 0) return;

    //Biến mặc định khi không được truyền vào
    if (!itemPerPage) itemPerPage = 10;
    if (!pageIndex) pageIndex = 1;

    //Tạo thanh loading trên đầu trang
    AjaxLoading(true);

    //Mở khối kết quả và phân trang
    $(".ajax-empty", $container).hide();
    $(".ajax-list", $container).show();
    $(".ajax-paging", $container).show();

    //Gửi dữ liệu lên server
    $.ajax({
        url: "/themes/themes01/ajax/ProductCart.aspx",
        type: "POST",
        dataType: "json",
        data: {
            "action": "GetMemberProductOrder",
            "itemPerPage": itemPerPage,
            "pageIndex": pageIndex
        },
        success: function (res) {
            AjaxLoading(false);

            if (res[0] === "success") {
                //Đổ phân trang
                $(".ajax-paging", $container).html(res[2]);

                //Đổ danh sách
                var productOrders = JSON.parse(res[1]);
                var productNames = JSON.parse(res[3]);

                if (productOrders.length > 0) {
                    //Xóa danh sách cũ
                    $(".ajax-list tbody", $container).html("");

                    //Lặp và xuất ra danh sách mới
                    for (var i = 0; i < productOrders.length; i++) {
                        FillOutMemberProductOrderHtml(productOrders[i], productNames[i], $container);
                    }
                } else {
                    //Thông báo chưa có kết quả
                    $(".ajax-empty", $container).show();
                    $(".ajax-list", $container).hide();
                    $(".ajax-paging", $container).hide();
                }
            }           
        },
        error: function (error) {
            AjaxLoading(false);
            console.log("Có lỗi xảy ra. GetMemberProductOrder");
        }
    });
}
// #endregion

// #region Điền thông tin bài viết từ object ra html - các giá trị trong data-role được fix cứng theo giá trị trong RoleHelper
function FillOutMemberProductOrderHtml(productOrder, productName, $container) {
   
    var productOrderHtml =
        '<tr>' +            
            '<td><a href="/thanh-vien/quan-ly-don-hang/' + productOrder.Code + '">' + productOrder.Code + '</a></td>' +
            '<td>' + Date.parse(productOrder.CreatedDate).toString("dd/MM/yyyy") + '</td>' +
            '<td>' + productName + '</td>' +            
            '<td class="text-right"><b>' + numberWithCommas(productOrder.ProductFee + productOrder.ShippingFee + productOrder.OtherFee, true) + ' đ</b></td>' +            
            '<td>' + GetMemberOrderStatusText(productOrder.OrderStatus) + '</td>' +
            '</tr>';

    $(".ajax-list tbody", $container).append(productOrderHtml);
}

function GetMemberOrderStatusText(orderStatusJsonString) {
    var s = "";

    try {
        var orderStatus = JSON.parse(orderStatusJsonString);
        if (orderStatus.length > 0) {

            switch (orderStatus[orderStatus.length - 1]) {
                case "0":
                    s = "<span class='text-warning'>Tiếp nhận đơn hàng</span>";
                    break;

                case "1":
                    s = "<span class='text-info'>Xác nhận đơn hàng</span>";
                    break;

                case "2":
                    s = "<span class='text-info'>Đã đóng gói</span>";
                    break;

                case "3":
                    s = "<span class='text-info'>Đã chuyển đơn vị vận chuyển</span>";
                    break;

                case "4":
                    s = "<span class='text-info'>Đang vận chuyển</span>";
                    break;

                case "5":
                    s = "<span class='text-success'>Giao hàng thành công</span>";
                    break;

                case "41":
                    s = "<span class='text-danger'>Hoãn giao hàng</span>";
                    break;

                case "51":
                    s = "<span class='text-danger'>Đơn hàng bị hủy</span>";
                    break;

                default:
                    s = status;
                    break;
            }
        }
    }
    catch (error) { }

    return s;
}

// #endregion
// #endregion


// #region Lấy thông tin chi tiết đơn hàng
function GetMemberProductOrderDetail(code) {
    //Khối chứa form
    var $container = $("#MemberProductOrderDetail");
    if ($container.length === 0) return;

    AjaxLoading(true);

    //Lấy dữ liệu đổ vào form
    $.ajax({
        url: "/themes/themes01/ajax/ProductCart.aspx",
        type: "POST",
        dataType: "json",
        data: {
            "action": "GetMemberProductOrderDetail",
            "code": code
        },
        success: function (res) {
            AjaxLoading(false);

            if (res[0] === "success") {
                var order = JSON.parse(res[1]);
                var orderDetails = JSON.parse(res[2]);

                //Thể hiện dữ liệu ra form
                // #region Mã đơn hàng, ngày đặt
                $("[data-name='Code']", $container).html(order.Code);
                $("[data-name='CreatedDate']", $container).html(Date.parse(order.CreatedDate).toString("dd/MM/yyyy - hh:mm tt"));
                // #endregion

                // #region Thông tin người nhận
                $("[data-name='MobileReceiver']", $container).html(order.MobileReceiver);
                $("[data-name='FullNameReceiver']", $container).html(order.FullNameReceiver);
                $("[data-name='EmailReceiver']", $container).html(order.EmailReceiver);
                $("[data-name='AddressReceiver']", $container).html(order.AddressReceiver);
                $("[data-name='DescReceiver']", $container).html(order.DescReceiver);
                // #endregion

                // #region Thông tin người đặt
                $("[data-name='Mobile']", $container).html(order.Mobile);
                $("[data-name='FullName']", $container).html(order.FullName);
                $("[data-name='Email']", $container).html(order.Email);
                // #endregion

                // #region Phương thức vận chuyển
                $("[data-name='DeliveryMethod']", $container).html(order.DeliveryMethod);
                $("[data-name='EstimatedDeliveryTime']", $container).val(order.EstimatedDeliveryTime);                
                // #endregion

                // #region Trạng thái thanh toán
                $("[data-name='PaymentMethod']", $container).html(order.PaymentMethod);
                // #endregion

                // #region Trạng thái xử lý
                $("[data-name='OrderStatus']", $container).html(GetMemberOrderStatusText(order.OrderStatus));
                // #endregion

                // #region Thể hiện thông tin danh sách sản phẩm trong giỏ hàng
                $(".order-detail-page-list", $container).html("");
                orderDetails.forEach(function (orderDetail) {
                    FillOutOrderDetailHtmlToOrderDetailPage(orderDetail, $container);
                });

                $("[data-name='ProductFee']", $container).html(numberWithCommas(order.ProductFee, true) + "đ");
                $("[data-name='ShippingFee']", $container).html(numberWithCommas(order.ShippingFee, true) + "đ");
                $("[data-name='OtherFee']", $container).html(numberWithCommas(order.OtherFee, true) + "đ");
                $("[data-name='TotalFee']", $container).html(numberWithCommas(order.ProductFee + order.ShippingFee + order.OtherFee, true) + "đ");
                // #endregion

            } else {
                $(".form-alert", $container).CreateFormAlert(res[1], false);
            }
        },
        error: function (error) {
            AjaxLoading(false);
            console.log("Có lỗi xảy ra. GetMemberProductOrderDetail");
        }
    });
}

// #region Thể hiện thông tin ở trang chi tiết giỏ hàng
function FillOutOrderDetailHtmlToOrderDetailPage(orderDetail, $container) {
    var orderDetailHtml =
        '<tr>' +
            '<td>' +
                '<div class="row no-gutters">' +
                    '<div class="col-auto">' +
                        '<div class="imgcw mr-2">' +
                            '<a target="_blank" class="imgc0" href="' + WebUrl + orderDetail.ProductSeoUrl + '" title="' + orderDetail.ProductTitle + '">' +
                                GetImageTag(orderDetail.ProductImage, orderDetail.ProductTitle, orderDetail.ProductImageApp, "Thumb", "", "") +
                            '</a>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col">' +
                        '<a target="_blank" href="' + WebUrl + orderDetail.ProductSeoUrl + '" title="' + orderDetail.ProductTitle + '" class="item-title">' + orderDetail.ProductTitle + '</a>' +
                        '<p class="item-slogan">' + ShowOrderDetailVariationInfo(orderDetail.Desc) + '</p>' +
                    '</div>' +
                '</div>' +
            '</td>' +
            '<td>' + numberWithCommas(orderDetail.Price, true) + 'đ</td>' +
            '<td>' + numberWithCommas(orderDetail.Quantity, true) + '</td>' +
            '<td class="text-right">' + numberWithCommas(orderDetail.Price * orderDetail.Quantity, true) + 'đ</td>' +
        '</tr>';


    $(".order-detail-page-list", $container).append(orderDetailHtml);
}
// #endregion

// #endregion



//Lấy danh sách đại lý ở trang giở hàng
if (typeof (PostSearchAgencyInBookingForm) == "function") {
    PostSearchAgencyInBookingForm();
}

//Lấy thông tin đơn hàng ở trang chi tiết
if (typeof (GetMemberProductOrder) == "function") {
    var pageIndex = $("#hdPageIndex").val();
    GetMemberProductOrder(10, pageIndex);
}

if (typeof (GetMemberProductOrderDetail) == "function") {
    var code = $("#hdOrderCode").val();
    GetMemberProductOrderDetail(code);
}