﻿/*Dropdown Hòa viết - 2018/05/22
//Khởi tạo. Nếu có data-dropdown-multiple="true" thì tự khởi tạo cho phép chọn nhiều mục
$(".dropdown").dropdown();

//Gán giá trị với drowndown chỉ cho chọn mục cùng lúc
$(".dropdown").dropdown("set", "Bóng đá");

// #region Gán giá trị cho dropdown có data-dropdown-multiple="true" - cho phép chọn nhiều mục cùng lúc
var SelectedIdArray = [];
SelectedIdArray.push(Giá trị 1);
SelectedIdArray.push(Giá trị 2);
$(".dropdown").dropdown("set", SelectedIdArray);
// #endregion

//Lấy giá trị. Nếu dropdown có data-dropdown-multiple="true" sẽ trả về mảng
$(".dropdown").dropdown("get");

//Sự kiện khi người dùng click chọn một item trong dropdown
$(".dropdown").on("hoa.selected.dropdown", function(e) {
    console.log(e.SelectedValue);
});
 
//Form mẫu
<div class="form-group">
    <label for="btCate">Danh mục</label>
    <div class="dropdown" data-selected-index="0" data-selected-value="">
        <button class="btn dropdown-toggle" id="btCate" type="button">
            <span class="dropdown-toggle-text ">Danh mục</span>
        </button>

        <div class="dropdown-menu">
            <div class="dropdown-title">
                <span>Danh mục bài viết</span>
                <button class="dropdown-title-button dropdown-menu-close"><i class="fa fa-times"></i></button>
            </div>
            <div class="dropdown-input">
                <input type="text" class="dropdown-input-field" value="" placeholder="Tìm kiếm"/>
            </div>
            <div class="dropdown-content">
                <ul>
                    <li><a href="#" data-val="Thời trang">Thời trang</a>
                        <ul>
                            <li><a href="#" data-val="Bộ sưu tập">Bộ sưu tập</a></li>
                            <li><a href="#" data-val="Xu hướng thời trang">Xu hướng thời trang</a></li>
                        </ul>
                    </li>
                    <li><a href="#" data-val="Thể thao">Thể thao</a>
                        <ul>
                            <li><a href="#" data-val="Bóng đá">Bóng đá</a></li>
                            <li><a href="#" data-val="Bơi lội">Bơi lội</a></li>
                            <li><a href="#" data-val="Quần vợt">Quần vợt</a></li>
                        </ul>
                    </li>
                    <li><a href="#" data-val="Văn hóa">Văn hóa</a></li>
                    <li><a href="#" data-val="Giải trí">Giải trí</a></li>
                </ul>
            </div>
            <div class="dropdown-loading">
                <i class="fa fa-spinner fa-spin"></i>
            </div>
        </div>
        <%--Chỉ thêm cho dropdown cho phép chọn nhiều mục cùng lúc--%>
        <div class="dropdown-selected form-group tag-wrap">
            <span>Nhóm đã chọn: </span>
            <div class="dropdown-selected-list"></div>
        </div>
    </div>
</div>
 */

(function ($) {
    //Hàm cập nhật lại các item được chọn ở dropdown multiple
    var updateMultipleSelectdItem = function ($this) {
        //Cập nhật các click ra khu .dropdown-selected-list
        $(".dropdown-selected-list", $this).html("");
        $(".dropdown-content a.active", $this)
            .each(function () {
                var selectedItem = '<a href="#" class="tag" data-val="' + $(this).attr("data-val") + '">' + $(this).text() + '<i class="fa fa-times"></i></a>';
                $(".dropdown-selected-list", $this).append(selectedItem);
            });

        //Gán sự kiện xóa khi click vào các tag đã chọn
        $(".dropdown-selected-list .tag", $this).click(function (event) {
            event.preventDefault();

            //Xóa tích ở dropdown-content
            $(".dropdown-content a[data-val='" + $(this).attr("data-val") + "']", $this).removeClass("active");

            //Tự xóa mình
            $(this).remove();
        });
    };

    var methods = {
        /* Khai báo tên các phương thức có thể gọi, bao gồm một init để khởi tạo và check để kiểm tra trước khi gửi lên server
         * $(selector).dropdown('init') //khởi tạo mặc định
         * $(selector).dropdown('init',{option}) //khởi tạo với options
         * $(selector).dropdown() //khởi tạo mặc định
         * $(selector).dropdown({option}) //khởi tạo với options
         */
        init: function (options) {
            return this.each(function () {
                var settings = $.extend({
                
                    },
                    options);

                //Gán settings vào data có tên dropdown của đối tượng này để dùng tới khi gọi các hàm liên quan
                $(this).data('dropdown', settings);

                initDropdown(this, settings);
            });
        },


        //Hàm lấy giá trị được chọn trong dropdown
        get: function () {
            var result;
            this.each(function () {
                var $this = $(this);

                //Lấy dữ liệu settings được lưu ở data dropdown của đối tượng này
                var settings = $this.data('dropdown');
                if (typeof settings !== 'object') {
                    $.error("Bạn cần khởi tạo với hàm dropdown('init', {options}) trước khi gọi được hàm này");
                }

                if (!$this.attr("data-dropdown-multiple")) {                    
                    result = $(".dropdown-content a.active", $this).attr("data-val");
                } else {
                    result = [];

                    $(".dropdown-content a.active", $this)
                        .each(function() {
                            result.push($(this).attr("data-val"));
                        });
                }
            });

            return result;
        },

        //Hàm gán giá trị đang được tích
        set: function (valueIn) {
            this.each(function () {
                var $this = $(this);

                //Lấy dữ liệu settings được lưu ở data dropdown của đối tượng này
                var settings = $this.data('dropdown');
                if (typeof settings !== 'object') {
                    $.error("Bạn cần khởi tạo với hàm dropdown('init', {options}) trước khi gọi được hàm này");
                }

                if (!$this.attr("data-dropdown-multiple")) {
                    $(".dropdown-content a", $this).removeClass("active");
                    $(".dropdown-content a[data-val='" + valueIn + "']", $this).addClass("active");
                    $(".dropdown-toggle-text", $this).text($(".dropdown-content a.active", $this).text());
                } else {
                    $(".dropdown-content a", $this).removeClass("active");
                    for (var i = 0; i < valueIn.length; i++) {
                        $(".dropdown-content a[data-val='" + valueIn[i] + "']", $this).addClass("active");
                    }

                    updateMultipleSelectdItem($this);
                }
            });
        }
    };


    //Hàm chính có tên dropdown    
    $.fn.dropdown = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }
        if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        }
        $.error('Phương thức "' + method + '" không được hỗ trợ bởi dropdown()');
    }

    //Khởi tạo dropdown
    function initDropdown(elm, settings) {
        var $this = $(elm);

        // #region Hủy sự kiện click vào chính nó để khởi tạo theo hàm ở dưới
        $this.click(function (event) { event.stopPropagation(); });
        // #endregion

        // #region Click vào nút đóng
        $(".dropdown-title .dropdown-menu-close", $this).unbind("click")
            .click(function (event) {
                event.preventDefault();

                $(".dropdown-menu", $this).hide();
            });
        // #endregion

        // #region Click vào nút dropdown thì mở dropdown-menu và focus vào ô tìm kiếm
        $(".dropdown-toggle", $this).unbind("click")
            .click(function (event) {
                event.preventDefault();

                $(".dropdown-menu", $this).toggle();

                $(".dropdown-input .dropdown-input-field", $this).focus();
            });
        // #endregion

        // #region Lọc kết quả tìm kiếm
        $(".dropdown-input .dropdown-input-field", $this).unbind("keyup").keyup(function () {
            var value = $(this).val().toLowerCase();

            $(".dropdown-content a", $this).filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
        // #endregion

        // #region Đánh dấu item được chọn, khi lấy kết quả sẽ lấy theo thuộc tính data-val của mục được chọn
        if (!$this.attr("data-dropdown-multiple")) {
            $(".dropdown-content a", $this)
                .unbind("click")
                .click(function(event) {
                    event.preventDefault();

                    $(".dropdown-content a", $this).removeClass("active");
                    $(this).addClass("active");

                    $(".dropdown-toggle-text", $this).text($(this).text());

                    //Trigger event để gọi tới khi click chọn một giá trị
                    $(this).trigger($.Event('hoa.selected.dropdown', { SelectedValue: $(this).attr("data-val") }));
                });
        } else {
            $(".dropdown-content a", $this)
                .unbind("click")
                .click(function (event) {
                    event.preventDefault();

                    //$(".dropdown-content a", $this).removeClass("active");
                    $(this).addClass("active");

                    //Trigger event để gọi tới khi click chọn một giá trị
                    $(this).trigger($.Event('hoa.selected.dropdown', { SelectedValue: $(this).attr("data-val") }));

                    updateMultipleSelectdItem($this);
                });
        }
        // #endregion

        // #region Nếu dropdown có thuộc tính data-selected-index thì tự chọn item theo chỉ số này
        if ($this.attr("data-selected-index")) {
            var selectedIndex = $this.attr("data-selected-index");
            $(".dropdown-content a", $this).removeClass("active");

            if ($(".dropdown-content a", $this).length > selectedIndex)
                $($(".dropdown-content a", $this)[selectedIndex]).addClass("active");

            $(".dropdown-toggle-text", $this).text($(".dropdown-content a.active", $this).text());
        }
        // #endregion

        // #region Nếu dropdown có thuộc tính data-selected-value thì tự chọn item theo giá trị này
        if ($this.attr("data-selected-value")) {
            var selectedValue = $this.attr("data-selected-value");

            if (selectedValue.length > 0) {
                $(".dropdown-content a", $this).removeClass("active");

                $(".dropdown-content a[data-val='" + selectedValue + "']", $this).addClass("active");

                $(".dropdown-toggle-text", $this).text($(".dropdown-content a.active", $this).text());
            }
        }
        // #endregion
    }
}(jQuery));