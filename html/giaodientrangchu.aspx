﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="giaodientrangchu.aspx.cs" Inherits="html_giaodientrangchu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta charset="utf-8" />
    <title>Giao diện trang chủ - menson</title>
    <link href="Assets/css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="Assets/js/slick-1.8.1/slick/slick.min.css" rel="stylesheet" />
    <link href="Assets/js/slick-1.8.1/slick/slick-theme.css" rel="stylesheet" />
    <link href="Assets/css/KhungAnh.css" rel="stylesheet" />
    <link href="Assets/css/giaodientrangchu.min.css" rel="stylesheet" />
    <link href="Assets/css/aos.css" rel="stylesheet" />
    <link href="Assets/css/Responsive.min.css" rel="stylesheet" />
    <script src="Assets/js/jquery-3.2.1.min.js"></script>
    <script src="Assets/js/slick-1.8.1/slick/slick.min.js"></script>
    <script src="Assets/js/aos.js"></script>
    <script src="Assets/js/main.js"></script>
    <script src="Assets/js/giaodientrangchu.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="header-content" data-aos="fade-down" data-aos-duration="2000">
            <div class="header-top">
                <div class="container">
                    <div class="logo">
                        <a href="#" title="logo Menson">
                            <img src="Assets/css/images/logo.jpg" />
                        </a>
                    </div>
                    <div class="header-right">
                        <ul class="contact">
                            <li><i class="fa fa-map-marker icon"></i> 104 Quán Thánh, Ba Đình, Hà Nội. Hotline: 
                                <a href="#" title="Hotline" class="tel">
                                    0969.235.777
                                </a>
                            </li>
                        </ul>
                        <div class="search-box">
                            <input class="text-search-box" type="type" name="name" placeholder="Từ khóa tìm kiếm"/>
                            <i class="fa fa-search icon-search"></i>
                        </div>
                        <div class="gio-hang">
                            <a href="#" title="Mua hàng">
                                <i class="fa fa-shopping-bag"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu">
                <div class="container">
                    <div class="menu-left">
                        <ul>
                            <a href="#" title="Trang chủ"><li>Trang chủ</li></a> 
                            <a href="#" title="Sơ mi"><li>Sơ mi <i class="fa fa-chevron-down"></i></li></a> 
                            <a href="#" title="Quần âu"><li>Quần âu <i class="fa fa-chevron-down"></i></li></a> 
                            <a href="#" title="Áo polo"><li>Áo polo <i class="fa fa-chevron-down"></i></li></a> 
                            <a href="#" title="Áo nỉ"><li>Áo nỉ <i class="fa fa-chevron-down"></i></li></a> 
                            <a href="#" title="Denim"><li>Denim <i class="fa fa-chevron-down"></i></li></a> 
                            <a href="#" title="Áo khoác"><li>Áo khoác <i class="fa fa-chevron-down"></i></li></a> 
                            <a href="#" title="Phụ kiện"><li>Phụ kiện</li></a> 
                        </ul>
                    </div>
                   
                    <div class="menu-right">
                        <ul>
                            <a href="#" title="Mới tuần này"><li>MỚI TUẦN NÀY</li></a> 
                            <a href="#" title="Khuyến mại"><li>KHUYẾN MẠI</li></a> 
                        </ul>
                    </div>
                </div>
            </div>
            <div class="menu-mobile">
                <div class="menu-wrap">
				<nav class="menu-tab">
					<div class="icon-list">
						<a href="#" title="Trang chủ"><li>Trang chủ</li></a> 
                        <a href="#" title="Sơ mi"><li>Sơ mi <i class="fa fa-chevron-down"></i></li></a> 
                        <a href="#" title="Quần âu"><li>Quần âu <i class="fa fa-chevron-down"></i></li></a> 
                        <a href="#" title="Áo polo"><li>Áo polo <i class="fa fa-chevron-down"></i></li></a> 
                        <a href="#" title="Áo nỉ"><li>Áo nỉ <i class="fa fa-chevron-down"></i></li></a> 
                        <a href="#" title="Denim"><li>Denim <i class="fa fa-chevron-down"></i></li></a> 
                        <a href="#" title="Áo khoác"><li>Áo khoác <i class="fa fa-chevron-down"></i></li></a> 
                        <a href="#" title="Phụ kiện"><li>Phụ kiện</li></a> 
					</div>
				</nav>
				<p class="close-button" id="close-button"><i class="fa fa-bar-chart-o"></i></p>
			</div>
	    <p class="menu-button" id="open-button"></p>
            </div>
        </div>
        <div class="slide"  data-aos="fade-down" data-aos-duration="2000">
            <div class="slider">
                <a href="#" title="slide1">
                    <div class="ban-chay trai">
                        <p class="summer">SUMMER 2018</p>
                        <p class="san-pham-ban-chay">SẢN PHẨM BÁN CHẠY NHẤT</p>
                    </div>
                    <img src="Assets/css/images/slide1.jpg" />
                </a>
                <a href="#" title="slide2">
                    <div class="ban-chay phai">
                        <p class="summer">SUMMER 2018</p>
                        <p class="san-pham-ban-chay">SẢN PHẨM BÁN CHẠY NHẤT</p>
                    </div>
                    <img src="Assets/css/images/slide2.jpg" />
                </a>
            </div>
        </div>
        <div class="san-pham" data-aos="fade-down" data-aos-duration="2000">
            <div class="big" data-aos="fade-right" data-aos-duration="500">
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Sơ Mi">
                            <li>
                                <img src="Assets/css/images/so-mi.jpg" />
                                <div class="caption">
                                    <div class="blur"></div>
                                    <div class="caption-text">
                                        <P class="so-mi">SƠ MI</P>
						                <a class="xem-chi-tiet" href="#" title="Xem chi tiết">
                                            Xem chi tiết <i class="fa fa-chevron-right icon"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Quần Âu">
                            <li>
                                <img src="Assets/css/images/quan.jpg" />
                                <div class="caption">
                                    <div class="blur"></div>
                                    <div class="caption-text">
                                        <P class="so-mi">QUẦN ÂU</P>
						                <a class="xem-chi-tiet" href="#" title="Xem chi tiết">
                                            Xem chi tiết <i class="fa fa-chevron-right icon"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </a>
                    </div>
                </div>
            </div>
            <div class="small" data-aos="fade-left" data-aos-duration="500">
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo Phông Polo">
                            <li class="phu-kien">
                                <img src="Assets/css/images/ao-phong-polo.jpg" />
                                <div class="caption">
                                    <div class="blur"></div>
                                    <div class="caption-text">
                                        <P class="so-mi">ÁO PHÔNG POLO</P>
                                        <a class="xem-chi-tiet" href="#" title="Xem chi tiết">
                                            Xem chi tiết <i class="fa fa-chevron-right icon"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo Khoác">
                            <li class="phu-kien">
                                <img src="Assets/css/images/ao-khoac.jpg" />
                                <div class="caption">
                                    <div class="blur"></div>
                                    <div class="caption-text">
                                        <P class="so-mi">ÁO KHOÁC</P>
                                        <a class="xem-chi-tiet" href="#" title="Xem chi tiết">
                                            Xem chi tiết <i class="fa fa-chevron-right icon"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" title="Phụ kiện" class="KhungAnhCrop">
                            <li class="phu-kien">
                                <img src="Assets/css/images/phu-kien.jpg" />
                                <div class="caption">
                                    <div class="blur"></div>
                                    <div class="caption-text">
                                        <P class="so-mi">PHỤ KIỆN</P>
                                        <a class="xem-chi-tiet" href="#" title="Xem chi tiết">
                                            Xem chi tiết <i class="fa fa-chevron-right icon"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="san-pham-noi-bat" data-aos="fade-up" data-aos-duration="2000">
            <a href="#" class="ten">
                Sản phẩm nổi bật
            </a>
            <div class="slide-mobile">
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham1.jpg" />
                        </a>
                        <p class="gia-dac-biet">GIÁ ĐẶC BIỆT</p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham2.jpg" />
                        </a>
                        <p class="gia-dac-biet">GIÁ ĐẶC BIỆT</p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham3.jpg" />
                        </a>
                        <p class="gia-dac-biet">GIÁ ĐẶC BIỆT</p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham4.jpg" />
                        </a>
                        <p class="gia-dac-biet" style="background-color:white;"></p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
            </div>
            <div class="slider2">
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham1.jpg" />
                        </a>
                        <p class="gia-dac-biet">GIÁ ĐẶC BIỆT</p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham2.jpg" />
                        </a>
                        <p class="gia-dac-biet">GIÁ ĐẶC BIỆT</p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham3.jpg" />
                        </a>
                        <p class="gia-dac-biet">GIÁ ĐẶC BIỆT</p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham4.jpg" />
                        </a>
                        <p class="gia-dac-biet" style="background-color:white;"></p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham1.jpg" />
                        </a>
                        <p class="gia-dac-biet" style="background-color:white;"></p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham2.jpg" />
                        </a>
                        <p class="gia-dac-biet" style="background-color:white;"></p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham3.jpg" />
                        </a>
                        <p class="gia-dac-biet" style="background-color:white;"></p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
                <div class="item">
                    <div class="KhungAnh">
                        <a href="#" class="KhungAnhCrop" title="Áo len bện sợi pha màu">
                           <img src="Assets/css/images/san-pham4.jpg" />
                        </a>
                        <p class="gia-dac-biet" style="background-color:white;"></p>
                        <p class="ten-san-pham">ÁO LEN BỆN SỢI PHA MÀU</p>
                        <p class="gia">5.200.000 đ</p>
                        <a href="#" title="Mua ngay">
                            <p class="button">
                                MUA NGAY
                            </p> 
                        </a>
                    </div>    
                </div>
            </div>
        </div>
        <div class="footer-pc">
                <div id="on-top">
                    <a href="#" title="Click to top">
                        <img src="Assets/css/images/on-top.png" />
                    </a>
                </div>
            <div class="container">
                <div class="item">
                    <div class="dang-ki-email">
                        <p class="tieu-de">ĐĂNG KÍ NHẬN BẢN TIN</p>
                        <p class="tin">Nhận tin tức và khuyến mãi mới <br /> nhất sẽ được gửi đến hộp thư của bạn</p>
                        <div class="email">
                            <input class="nhap-email" type="email" placeholder="Địa chỉ Email" title="Nhập Email"/>
                            <button class="dangki" title="Đăng ký"">ĐĂNG KÝ</button>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="chung">
                        <p class="tieu-de">CHÍNH SÁCH BÁN HÀNG</p>
                        <a href="#" class="chinh-sach-ban-hang" title="Hướng dẫn mua hàng">
                            <p>Hướng dẫn mua hàng</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Chính sách giao nhận">
                            <p>Chính sách giao nhận</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Chính sách đổi trả">
                            <p>Chính sách đổi trả</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Bảo mật thông tin khách hàng">
                            <p>Bảo mật thông tin khách hàng</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="men">
                        <p class="tieu-de menson">MENSON</p>
                        <a href="#" class="chinh-sach-ban-hang" title="Tòa nhà 34T, Hoàng Đạo Thúy, Hà Nội">
                            <p><i class="fa fa-map-marker icon"></i> Tòa nhà 34T, Hoàng Đạo Thúy, Hà Nội</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Hotline: 0981.555.379">
                            <p><i class="fa fa-phone icon"></i> Hotline: 0981.555.379</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Email: menson@gmail.com">
                            <p><i class="fa fa-envelope icon"></i> Email: menson@gmail.com</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Website: www.menson.com">
                            <p><i class="fa fa-globe icon"></i> Website: www.menson.com</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="chung">
                        <p class="tieu-de">
                            Menson trên 
                            <a href="#" title="Facebook">
                                <i class="fa fa-facebook-square icon"></i>
                            </a>
                            <a href="#" title="Youtube">
                                <i class="fa fa-youtube-play icon"></i>
                            </a>
                            <a href="#" title="Google">
                                <i class="fa fa-google-plus-square icon"></i>
                            </a>
                            <a href="#" title="Twitter">
                                <i class="fa fa-twitter-square icon"></i>
                            </a>
                        </p>
                        <div class="hotline">
                            <ul>
                                <li>
                                    <i class="fa fa-phone phone"></i>
                                </li>
                                <li>
                                    <p class="tu-van">Hotline tư vấn bán hàng:</p>
                                    <p class="number">0969.235.777 - 0969.235.777</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="chat">
                <a href="#" title="Open Chat Facebook">
                    <i class="fa fa-facebook-f facebook"><span class="open">Open Chat Facebook</span></i>
                </a>
            </div>
            
        </div>
        <div class="footer-tab">
                <div id="on-top">
                    <a href="#" title="Click to top">
                        <img src="Assets/css/images/on-top.png" />
                    </a>
                </div>
            <div class="container">
                <div class="up">
                    <div class="item">
                    <div class="dang-ki-email">
                        <p class="tieu-de">ĐĂNG KÍ NHẬN BẢN TIN</p>
                        <p class="tin">Nhận tin tức và khuyến mãi mới nhất sẽ được gửi <br />  đến hộp thư của bạn</p>
                        <div class="email">
                            <input class="nhap-email" type="email" placeholder="Địa chỉ Email" title="Nhập Email"/>
                            <button class="dangki" title="Đăng ký">ĐĂNG KÝ</button>
                        </div>
                    </div>
                    <div class="men">
                        <p class="tieu-de menson">MENSON</p>
                        <a href="#" class="chinh-sach-ban-hang" title="Tòa nhà 34T, Hoàng Đạo Thúy, Hà Nội">
                            <p><i class="fa fa-map-marker icon"></i> Tòa nhà 34T, Hoàng Đạo Thúy, Hà Nội</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Hotline: 0981.555.379">
                            <p><i class="fa fa-phone icon"></i> Hotline: 0981.555.379</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Email: menson@gmail.com">
                            <p><i class="fa fa-envelope icon"></i> Email: menson@gmail.com</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Website: www.menson.com">
                            <p><i class="fa fa-globe icon"></i> Website: www.menson.com</p>
                        </a>
                    </div>
                </div>
                    <div class="item">
                    <div class="chung">
                        <p class="tieu-de">CHÍNH SÁCH BÁN HÀNG</p>
                        <a href="#" class="chinh-sach-ban-hang" title="Hướng dẫn mua hàng">
                            <p>Hướng dẫn mua hàng</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Chính sách giao nhận">
                            <p>Chính sách giao nhận</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Chính sách đổi trả">
                            <p>Chính sách đổi trả</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Bảo mật thông tin khách hàng">
                            <p>Bảo mật thông tin khách hàng</p>
                        </a>
                    </div>
                </div>
                </div>
                <div class="down">
                <div class="item">
                    <div class="chung">
                        <p class="tieu-de">
                            Menson trên 
                            <a href="#" title="Facebook">
                                <i class="fa fa-facebook-square icon"></i>
                            </a>
                            <a href="#" title="Youtube">
                                <i class="fa fa-youtube-play icon"></i>
                            </a>
                            <a href="#" title="Google">
                                <i class="fa fa-google-plus-square icon"></i>
                            </a>
                            <a href="#" title="Twitter">
                                <i class="fa fa-twitter-square icon"></i>
                            </a>
                        </p>
                        <div class="hotline">
                            <ul>
                                <li>
                                    <i class="fa fa-phone phone"></i>
                                </li>
                                <li>
                                    <p class="tu-van">Hotline tư vấn bán hàng:</p>
                                    <p class="number">0969.235.777 - 0969.235.777</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="chat">
                <a href="#" title="Open Chat Facebook">
                    <i class="fa fa-facebook-f facebook"><span class="open">Open Chat Facebook</span></i>
                </a>
            </div>
        </div>
        <div class="footer-mobile">
                <div id="on-top">
                    <a href="#" title="Click to top">
                        <img src="Assets/css/images/on-top.png" />
                    </a>
                </div>
            <div class="container">
                    <div class="item">
                    <div class="dang-ki-email">
                        <p class="tieu-de">ĐĂNG KÍ NHẬN BẢN TIN</p>
                        <p class="tin">Nhận tin tức và khuyến mãi mới nhất sẽ được gửi đến hộp thư của bạn</p>
                        <div class="email">
                            <input class="nhap-email" type="email" placeholder="Địa chỉ Email" title="Nhập Email"/>
                            <button class="dangki" title="Đăng ký">ĐĂNG KÝ</button>
                        </div>
                    <div class="men">
                        <p class="tieu-de menson">MENSON</p>
                        <a href="#" class="chinh-sach-ban-hang" title="Tòa nhà 34T, Hoàng Đạo Thúy, Hà Nội">
                            <p><i class="fa fa-map-marker icon"></i> Tòa nhà 34T, Hoàng Đạo Thúy, Hà Nội</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Hotline: 0981.555.379">
                            <p><i class="fa fa-phone icon"></i> Hotline: 0981.555.379</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Email: menson@gmail.com">
                            <p><i class="fa fa-envelope icon"></i> Email: menson@gmail.com</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Website: www.menson.com">
                            <p><i class="fa fa-globe icon"></i> Website: www.menson.com</p>
                        </a>
                    </div>
                </div>
                    <div class="item">
                    <div class="chung">
                        <p class="tieu-de">CHÍNH SÁCH BÁN HÀNG</p>
                        <a href="#" class="chinh-sach-ban-hang" title="Hướng dẫn mua hàng">
                            <p>Hướng dẫn mua hàng</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Chính sách giao nhận">
                            <p>Chính sách giao nhận</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Chính sách đổi trả">
                            <p>Chính sách đổi trả</p>
                        </a>
                        <a href="#" class="chinh-sach-ban-hang" title="Bảo mật thông tin khách hàng">
                            <p>Bảo mật thông tin khách hàng</p>
                        </a>
                    </div>
                </div>
                </div>
            <div class="item">
                        <p class="tieu-de">
                            Menson trên 
                            <a href="#" title="Facebook">
                                <i class="fa fa-facebook-square icon"></i>
                            </a>
                            <a href="#" title="Youtube">
                                <i class="fa fa-youtube-play icon"></i>
                            </a>
                            <a href="#" title="Google">
                                <i class="fa fa-google-plus-square icon"></i>
                            </a>
                            <a href="#" title="Twitter">
                                <i class="fa fa-twitter-square icon"></i>
                            </a>
                        </p>
                        <div class="hotline">
                            <ul>
                                <li>
                                    <i class="fa fa-phone phone"></i>
                                </li>
                                <li>
                                    <p class="tu-van">Hotline tư vấn bán hàng:</p>
                                    <p class="number">0969.235.777 - 0969.235.777</p>
                                </li>
                            </ul>
                        </div>
                   
                </div>
                
            </div>
            <div class="chat">
                <a href="#" title="Open Chat Facebook">
                    <i class="fa fa-facebook-f facebook"><span class="open">Open Chat Facebook</span></i>
                </a>
            </div>
        </div>
    </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.slider').slick({
                    arrows: true,
                    autoplay: true,
                    autoplaySpeed: 2000,
                });
                $('.slider2').slick({
                    arrows: true,
                    autoplay: true,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    autoplaySpeed: 2000,
                });
                $("#on-top").hide();
                $(function () {
                    $(window).scroll(function () {
                        if ($(this).scrollTop() > 100) {
                            $('#on-top').fadeIn();
                        } else {
                            $('#on-top').fadeOut();
                        }
                    });
                    $('#on-top a').click(function () {
                        $('body,html').animate({
                            scrollTop: 0
                        }, 800);
                        return false;
                    });
                    $('#open-button').click(function () {
                        $('#open-button').hide();
                    })
                    $('#close-button').click(function () {
                        $('#open-button').show();
                    })
                });
                
            });
            AOS.init({
                once:true
            });
        </script>
        
    </form>
</body>
</html>
