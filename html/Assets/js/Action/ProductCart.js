﻿// #region Lấy thông tin giỏ hàng và thể hiện ở cả icon giỏ hàng và trang chi tiết giỏ hàng
function GetShoppingCartList() {
    /// <summary>Lấy thông tin giỏ hàng và thể hiện ở cả icon giỏ hàng và trang chi tiết giỏ hàng</summary>

    //Gửi dữ liệu lên server
    $.ajax({
        url: WebUrl + "/Themes/Themes01/Ajax/ProductCart.aspx",
        type: "POST",
        dataType: "json",
        data: {
            "action": "GetShoppingCartList"
        },
        success: function (res) {
            var orderDetails = JSON.parse(res[2]);

            // #region Thể hiện thông tin ở phần icon giỏ hàng trên header
            $(".shopping-cart-count").html(res[0]);
            $(".shopping-cart-total-product-fee").html(numberWithCommas(res[1], true));

            $(".shopping-cart-list").html("");
            orderDetails.forEach(function (orderDetail) {
                FillOutOrderDetailHtml(orderDetail);
            });
            // #endregion

            // #region Thể hiện thông tin ở trang chi tiết giỏ hàng
            $(".shopping-cart-page-count").html(res[0]);
            $(".shopping-cart-page-total-product-fee").html(numberWithCommas(res[1], true));

            $(".shopping-cart-page-list").html("");
            orderDetails.forEach(function (orderDetail) {
                FillOutOrderDetailHtmlToShoppingCartPage(orderDetail);
            });

            //Khởi tạo autoNumeric ở ô số lượng
            $(".shopping-cart-page-list .autoNumeric").autoNumeric("init", autoNumericSettings);
            // #endregion
        },
        error: function (error) {
            console.log("Có lỗi xảy ra. GetShoppingCartList");
        }
    });
}

// #region Thể hiện thông tin ở phần icon giỏ hàng trên header
function FillOutOrderDetailHtml(orderDetail) {
    var orderDetailHtml =
        '<div class="item">' +
            '<div class="imgcw">' +
                '<a class="imgc0" href="' + WebUrl + orderDetail.ProductSeoUrl + '" title="' + orderDetail.ProductTitle + '">' +
                    GetImageTag(orderDetail.ProductImage, orderDetail.ProductTitle, orderDetail.ProductImageApp, "Thumb", "", "") +
                '</a>' +
            '</div>' +
            '<div class="item-infos">' +
                '<a href="' + WebUrl + orderDetail.ProductSeoUrl + '" title="' + orderDetail.ProductTitle + '" class="item-title">' + orderDetail.ProductTitle + '</a>' +
                '<p class="item-slogan">' + ShowOrderDetailVariationInfo(orderDetail.Desc) + '</p>' +
                '<p class="item-price">Đơn giá: <span>' + numberWithCommas(orderDetail.Price, true) + 'đ</span>, Số lượng: <span>' + orderDetail.Quantity + '</span></p>' +
            '</div>' +
        '</div>';

    $(".shopping-cart-list").append(orderDetailHtml);
}

function ShowOrderDetailVariationInfo(variationInfoString, removeTitle) {
    var s = "";

    try {
        var variationInfos = JSON.parse(variationInfoString);
        variationInfos.forEach(function (variationInfo) {
            if (!removeTitle)
                s += variationInfo.Title + ": " + variationInfo.Value + ", ";
            else
                s += variationInfo.Value + ", ";
        });
    } catch (error) { }

    if (s.length > 0)
        s = s.substring(0, s.length - ", ".length);

    return s;
}
// #endregion


// #region Thể hiện thông tin ở trang chi tiết giỏ hàng
function FillOutOrderDetailHtmlToShoppingCartPage(orderDetail) {
    var orderDetailHtml =
        '<tr data-id="' + orderDetail.ProductId + '">' +
            '<td>' +
                '<div class="imgcw">' +
                    '<a class="imgc0" href="' + WebUrl + orderDetail.ProductSeoUrl + '" title="' + orderDetail.ProductTitle + '">' +
                        GetImageTag(orderDetail.ProductImage, orderDetail.ProductTitle, orderDetail.ProductImageApp, "Small", "", "") +
                    '</a>' +
                '</div>' +
            '</td>' +
            '<td>' +
                '<a href="' + WebUrl + orderDetail.ProductSeoUrl + '" title="' + orderDetail.ProductTitle + '" class="item-title">' + orderDetail.ProductTitle + '</a>' +
                '<p class="item-slogan">' + ShowOrderDetailVariationInfo(orderDetail.Desc) + '</p>' +
            '</td>' +
            '<td>' + ShowOrderDetailVariationInfo(orderDetail.Desc, true) + '</td>' +
            '<td data-val="' + orderDetail.Price + '" data-name="price"><b>' + numberWithCommas(orderDetail.Price, true) + 'đ</b></td>' +
            '<td>' +
                '<div class="div-quantity">' +
                    '<a href="javascript:void(0);" onclick="QuantityUp(this,-1)"><i class="fa fa-minus"></i></a>' +
                    '<input data-name="quantity" onchange="ChangeQuantityInShoppingCart(this, ' + orderDetail.ProductId + ')" type="text" class="autoNumeric" value="' + orderDetail.Quantity + '">' +
                    '<a href="javascript:void(0);" onclick="QuantityUp(this)"><i class="fa fa-plus"></i></a>' +
                '</div>' +
            '</td>' +
            '<td>' +
                '<a href="javascript:void(0);" onclick="RemoveProductInShoppingCart(' + orderDetail.ProductId + ')"><i class="fa fa-plus"></i></a>' +
            '</td>' +
        '</tr>';


    $(".shopping-cart-page-list").append(orderDetailHtml);
}
// #endregion

// #endregion