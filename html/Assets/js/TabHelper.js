﻿$(".tabs-wrap").each(function() {
    var $this = $(this);

    $(".tabs .tab", $this)
        .click(function(event) {
            event.preventDefault();

            var dataTab = $(this).attr("data-tab");

            $(".tabs .tab", $this).removeClass("active");
            $(".tabs-content .tab-content", $this).removeClass("active");

            $(".tabs .tab[data-tab='"+dataTab+"']", $this).addClass("active");
            $(".tabs-content .tab-content[data-tab='" + dataTab + "']", $this).addClass("active");


            //#region Khởi tạo lại slick và crop lại ảnh trong tab được mở
            //Hủy slick và gọi lại slick cho các khối có .slick-slider trong tab, thuộc tính lấy từ data-slick
            $(".tabs-content .tab-content[data-tab='" + dataTab + "'] .slick-slider", $this).slick("unslick").slick();;
        });
});

$(".tab-expaned-wrap").each(function () {
    var $this = $(this);

    $(".tab", $this)
        .click(function (event) {
            event.preventDefault();

            $this.toggleClass("expanded");

            if ($this.hasClass("expanded")) {
                //#region Khởi tạo lại slick và crop lại ảnh trong tab được mở
                //Hủy slick và gọi lại slick cho các khối có .slick-slider trong tab, thuộc tính lấy từ data-slick
                $(".tab-content .slick-slider", $this)
                    .slick("unslick")
                    .slick();;              
            }
        });
});